---
title: "Wat is CSS?"
weight: 100
draft: false
---
CSS is de afkorting van Cascading Style Sheets. Met alleen HTML kun je wel een website maken, maar dan ziet hij er niet goed uit. CSS regelt namelijk het uiterlijk van je webpagina. In plaats van tags in HTML heeft CSS **rules**. Hiermee kun je een HTML-element selecteren en stijlen. Een css rule bestaat uit een **selector** en meerdere **declaratoren** waar `{}` omheen staan. Een voorbeeld van een CSS-rule is

{{< highlight css >}}p {
    color: blue;
    background-color: white;
}{{< /highlight >}}

De `p` staat in dit css-voorbeeld voor een `<p>` in je HTML-code. Met deze selector selecteer je elke `<p>` in je html; hierna komt een `{`. Na het haakje komen we bij de selectoren. Hiermee kunnen we de eigenschappen van het door de selector geselecteerde tag(s) veranderen. `color: blue` verandert de kleur van de tekst en `background-color: white` de achtergrondkleur. Je moet niet vergeten na elke selector een `;` te plaatsen. Sluit de rule af met een `}`

Je kunt de css-styling op verschillende plekken plaatsen. Bijvoorbeeld in de `<head>` in een `<style>` (niet aangeraden) of in een apart bestand gelinkt naar je website in de `<head>` met `<link rel="stylesheet" href="css/styles.css">`

## Selectoren
Je kunt HTML-elementen selecteren bij tag, **ID** en **class**. Tags zijn in html `<p>` en in CSS `p`. Wat je met een ID kunt doen is een specifiek element op de pagina aanroepen. Een id in html ziet er zo uit: `<p id="paragraaf1">` en in CSS `#paragraaf1`. Een class doet precies hetzelfde als een ID, maar dan voor meerdere elementen. In html is een class `<p class="paragraaf">` en in css `.paragraaf`. Elementen in HTML kunnen meerdere tags en ID's hebben. `<p class="paragraaf blauwetekst" id="paragraaf1 scrollto1">` is een geldig gebruik van class en ID.