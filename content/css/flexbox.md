---
title: "Flexbox"
weight: 90
draft: false
---
## We gaan flexbox leren!
Flexbox is op deze website veel gebruikt, en dat heeft een reden. Flexbox, of eerder .je_element{display:flex;} in je css zorgt ervoor dat je elementen makkelijk kunt plaatsen op de plek die je wilt; Je kunt elementen makkelijk in het midden, aan het begin of aan het einde van het element plaatsen. Hieronder wordt snel een uitleg gegeven over de basis van het gebruik van flexbox, je kunt ook de verschillende opties interactief testen onderaan de pagina.
{{< rawhtml >}}
                        <div class="page__flexbox_control_container row-is-selected">
                                <div class="page__flexbox_control_heading">Hoeveelheid elementen (Maximum 100)</div>
                                <input id="page__flexbox_ChildCount" class="slider" type="number" min="0" max="100">
                        </div>
                        <div class="page__flexbox_control_container row-is-selected">
                                <div class="page__flexbox_control flex-direction">
                                        <div class="page__flexbox_control_heading">flex-direction: Verander de richting van de flexbox. Ga in kolommen of in rijen. De reverse-opties zetten de laatste elementen vooraan en de eerste achteraan.</div>
                                        <div class="page__flexbox_control_group">
                                                <label>
                                                        <input type="radio" name="flex-direction" value="row" checked="">
                                                        row (standaard)
                                                </label>
                                                <label>
                                                        <input type="radio" name="flex-direction" value="row-reverse">
                                                        row-reverse
                                                </label>
                                                <label>
                                                        <input type="radio" name="flex-direction" value="column">
                                                        column
                                                </label>
                                                <label>
                                                        <input type="radio" name="flex-direction" value="column-reverse">
                                                        column-reverse
                                                </label>
                                        </div>
                                </div>
                                <div class="page__flexbox_control flex-wrap">
                                        <div class="page__flexbox_control_heading">flex-wrap: Als er te weinig ruimte is voor de elementen, stuur ze naar de nieuwe regel.</div>
                                        <div class="page__flexbox_control_group">
                                                <label>
                                                        <input type="radio" name="flex-wrap" value="nowrap" checked="">
                                                        nowrap (standaard)
                                                </label>
                                                <label>
                                                        <input type="radio" name="flex-wrap" value="wrap">
                                                        wrap
                                                </label>
                                                <label>
                                                        <input type="radio" name="flex-wrap" value="wrap-reverse">
                                                        wrap-reverse
                                                </label>
                                        </div>
                                </div>
                                <div class="page__flexbox_control justify-content">
                                        <div class="page__flexbox_control_heading">justify-content: Hoe de elementen worden verspreid op de X-as als het een rijen-layout is.</div>
                                        <div class="page__flexbox_control_group">
                                                <label>
                                                        <input type="radio" name="justify-content" value="flex-start" checked="">
                                                        flex-start (standaard)
                                                </label>
                                                <label>
                                                        <input type="radio" name="justify-content" value="flex-end">
                                                        flex-end
                                                </label>
                                                <label>
                                                        <input type="radio" name="justify-content" value="center">
                                                        center
                                                </label>
                                                <label>
                                                        <input type="radio" name="justify-content" value="space-around">
                                                        space-around
                                                </label>
                                                <label>
                                                        <input type="radio" name="justify-content" value="space-between">
                                                        space-between
                                                </label>
                                        </div>
                                </div>
                                <div class="page__flexbox_control align-items">
                                        <div class="page__flexbox_control_heading">align-items: Hoe de elementen worden verspreid op de X-as als het een kolommen-layout is.</div>
                                        <div class="page__flexbox_control_group">
                                                <label>
                                                        <input type="radio" name="align-items" value="stretch" checked="">
                                                        stretch (standaard)
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-items" value="baseline">
                                                        baseline
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-items" value="center">
                                                        center
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-items" value="flex-start">
                                                        flex-start
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-items" value="flex-end">
                                                        flex-end
                                                </label>
                                        </div>
                                </div>
                                <div class="page__flexbox_control align-content">
                                        <div class="page__flexbox_control_heading">align-content: Ik weet niet wat deze doet</div>
                                        <div class="page__flexbox_control_group">
                                                <label>
                                                        <input type="radio" name="align-content" value="stretch" checked="">
                                                        stretch (standaard)
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-content" value="center">
                                                        center
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-content" value="flex-start">
                                                        flex-start
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-content" value="flex-end">
                                                        flex-end
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-content" value="space-around">
                                                        space-around
                                                </label>
                                                <label>
                                                        <input type="radio" name="align-content" value="space-between">
                                                        space-between
                                                </label>
                                        </div>
                                </div>
                        </div>
                        <div class="page__flexbox_learnflexpreview_container">
                                <div class="page__flexbox_control_heading">CSS-Uitvoer</div>
                                <pre class="page__flexbox_learnflexpreview code">{
	display: flex;
	flex-direction: row;
	flex-wrap: nowrap;
	justify-content: flex-start;
	align-items: stretch;
	align-content: stretch;
}</pre>
                        </div>
                        <div class="page__flexbox_learnflex_container" id="page__flexbox_LearnFlexContainer" style="flex-flow: row nowrap; place-content: stretch flex-start; align-items: stretch;">

                        </div>

								<script>
var learnFlexContainer = document.querySelector('.page__flexbox_learnflex_container');
var controlsContainer = document.querySelector('.page__flexbox_control_container');
var propertyInputs = Array.prototype.slice.call(document.querySelectorAll('input[type="radio"]'));

var preview = document.querySelector('.page__flexbox_learnflexpreview');
var styleObj = {
        display: 'flex'
};

var flexChildren = Array.prototype.slice.call(document.querySelectorAll('.page__flexbox_learnflex_container_child'));
var childCount = document.getElementById('page__flexbox_ChildCount');

var updateLearnFlexContainer = function(input) {
        learnFlexContainer.style[input.name] = input.value;
}

var updateExplanations = function() {
        var flexDirectionValue = document.querySelector('input[name="flex-direction"]:checked').value;

        // some parts of the explanation text depend on the flex-direction selection
        if (flexDirectionValue.match('row')) {
                // when `flex-direction: row`
                controlsContainer.classList.add('row-is-selected');
                controlsContainer.classList.remove('column-is-selected');
        } else if (flexDirectionValue.match('column')) {
                // when `flex-direction: column`
                controlsContainer.classList.add('column-is-selected');
                controlsContainer.classList.remove('row-is-selected');
        }
}

function updateCodePreview(input) {
        styleObj[input.name] = input.value;

        var previewCss = JSON.stringify(styleObj, null, '\t');
        preview.innerHTML = previewCss
                .replace(/"/g, '')
                .replace(/,/g, ';')
                .replace(/(align-content.*)(\n)/, '$1;\n')
}

var inputListener = function(evt) {
        updateLearnFlexContainer(evt.target);
        updateExplanations();
        updateCodePreview(evt.target);
};

// initial setup
propertyInputs.forEach(function(input) {
        // event listener for each input
        input.addEventListener('change', inputListener);
        // inputs might not be on the default when page loads
        if (input.checked) {
                updateLearnFlexContainer(input);
                updateCodePreview(input);
        }
});
childCount.value = 0;
// set up change of child counts
childCount.addEventListener('change', function(evt) {
        var maxcount = evt.target.value;
        var count = 0;

        var parent = document.getElementById('page__flexbox_LearnFlexContainer');
        while (parent.hasChildNodes()) {
                parent.removeChild(parent.firstChild);
        }
        while (count != maxcount) {
                var newChild = '<div class="page__flexbox_learnflex_container_child">' + (count+1) + '</div>';
                parent.insertAdjacentHTML('beforeend', newChild);
                count++;
        }
});
updateExplanations();
								</script>
{{< /rawhtml >}}
