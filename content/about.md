---
title: "About"
date: 2019-01-29T12:16:19+01:00
menu: "main"
weight: 1
coverimage: "img/jasperalberinglogo.svg"
draft: false
---
### Who I am

I am Jasper Albering. I am a student in middle school. I live in the Netherlands. I can speak fluent English, Dutch and some broken Polish. I like to program as my hobby. I am a beginner in C++, PHP, Go and like to write apps and games.
