---
title: "Playground"
draft: false
menu: "main"
weight: 3
nopostwrapper: true;
---

{{< rawhtml >}}
<div class="page__playground">
<div class="page__playground_box">
<button onclick="page__playground_update();"><i class="fas fa-cogs"></i> Uitvoeren</button>
<button onclick="page__playground_save();"><i class="fas fa-file-download"></i> Downloaden</button>
<div id="page__playground_input">&lt;!doctype html&gt;

&lt;html lang=&quot;nl&quot;&gt;
&lt;head&gt;
  &lt;meta charset=&quot;utf-8&quot;&gt;

  &lt;title&gt;Titel&lt;/title&gt;
  &lt;meta name=&quot;description&quot; content=&quot;Dit is een template voor html5&quot;&gt;
  &lt;link rel=&quot;stylesheet&quot; href=&quot;css/styles.css&quot;&gt;

&lt;/head&gt;

&lt;body&gt;
  &lt;script src=&quot;js/scripts.js&quot;&gt;&lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</div>
</div>
<div class="page__playground_handle"></div>
<div class="page__playground_box">
<iframe id="page__playground_render" frameborder="0" target="_top"></iframe>
</div>
</div>
    
<script src="//ajaxorg.github.io/ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="//ajaxorg.github.io/ace-builds/src-min-noconflict/ext-language_tools.js" type="text/javascript" charset="utf-8"></script>
<script>
    ace.require("ace/ext/language_tools");
    var editor = ace.edit("page__playground_input");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/html");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true,
        maxLines: Infinity,
        minLines: 40
    });
function page__playground_update()
{
var doc = document.getElementById('page__playground_render').contentWindow.document;
doc.open();
doc.write(editor.getValue());
doc.close();
}
var handle = document.querySelector('.page__playground_handle');
var wrapper = handle.closest('.page__playground');
var box = wrapper.querySelector('.page__playground_box');
var isHandleDragging = false;

document.addEventListener('mousedown', function(e) {
  if (e.target === handle) {
    isHandleDragging = true;
    document.getElementById("page__playground_render").style.pointerEvents = "none";
  }
});

document.addEventListener('mousemove', function(e) {
  if (!isHandleDragging) {
    return false;
  }

  var containerOffsetLeft = wrapper.offsetLeft;

  var pointerRelativeXpos = e.clientX - containerOffsetLeft;
  var boxminWidth = 60;

  box.style.width = (Math.max(boxminWidth, pointerRelativeXpos - 4)) + 'px';
  box.style.flexGrow = 0;
  window.dispatchEvent(new Event('resize'))
});

document.addEventListener('mouseup', function(e) {
  isHandleDragging = false;
  document.getElementById("page__playground_render").style.pointerEvents = "";
});

function page__playground_save()
{
    var textToSave = editor.getValue();
    var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = "index.html";

    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);

    downloadLink.click();
}

function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}

</script>
{{< /rawhtml >}}