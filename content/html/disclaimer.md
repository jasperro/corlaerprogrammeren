--- 
title: "De Disclaimer"
weight: 80
draft: false
---
Voordat je begint met je website te bouwen, moet je even deze **disclaimer** lezen.

De regels zijn simpel:

1. Weet wat je doet: kopieër pas van andere websites als je precies weet wat het doet.
2. Dat was het. ***Weet wat je doet!***