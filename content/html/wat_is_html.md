---
title: "Wat is HTML?"
weight: 100
draft: false
---
## Inleiding
HTML is de afkorting van *Hyper Text Markup Language*, wat vertaald naar het Nederlands *Hypertekst Opmaaktaal* zou zijn. Het woord opmaaktaal is hierin heel belangrijk. HTML is geen programmeertaal, maar een **opmaaktaal**. HTML bepaalt op jouw website alle inhoud. Als je bijvoorbeeld een krant hebt, zijn alle afbeeldingen en teksten op hun eigen plek gezet. Dat doet HTML precies hetzelfde, maar dan voor websites.

## Tags

Nu dat je weet wat HTML is, is het ook handig dat je weet hoe je het moet gebruiken. HTML is opgebouwd uit verschillende **tags**. Dat zijn de **openingshaakjes** `<>` en **afsluithaakjes** `</>` met tekst ertussen. Dus bijvoorbeeld `<p></p>` voor een paragraph, dus waar je tekst in kunt plaatsen of `<h1></h1>` wat voor headings, of titeltekst is. De browser leest deze tags en plaatst ze in volgorde op je scherm. Bijna alle tags staan [hier](/html/tags) op een rijtje.

{{< highlight html "linenos=table,linenostart=0" >}}
<!doctype html>

<html lang="nl">
<head>
  <meta charset="utf-8">

  <title>Titel</title>
  <meta name="description" content="Dit is een template voor html5">
  <link rel="stylesheet" href="css/styles.css">

</head>

<body>
  <script src="js/scripts.js"></script>
</body>
</html>
{{< / highlight >}}

## Opbouw

De eerste tag die je ziet is `<!doctype html>`. Die zegt tegen de browser dat het document een html-document is. Zonder deze kunnen er problemen ontstaan en werken emoji's en andere unicode-karakters niet. De volgende tag die we in het document zien is `<html lang="nl">`. Deze tag bevat alle andere tags. Hierna komen `<head>` en `<body>`. De head bevat alles dat je niet wilt laten zien, en de body is wat op je beeldscherm wordt weergeven. De head bevat bijvoorbeeld je css, de titel van de pagina, het icoontje, en meta-tags die bijvoorbeeld zoekmachines zoals Google kunnen gebruiken om je website makkelijker te scannen. In de body gebeurt het meeste van je website. Daar komen je menubalk, tekst, plaatjes en meer te staan.

## Attributen

HTML-elementen kunnen **attributen** hebben. Dat zijn stukjes tekst in je tag, met een = en "". Het element `<html lang="nl">` bevat een attribuut. Dat attribuut zegt dat de taal van de website Nederlands is. De meest gebruikte attributen zijn id en class, daar kom je meer over te weten in het CSS-hoofdstuk.