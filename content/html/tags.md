--- 
title: "Tags"
weight: 90
draft: false
---
Html heeft heel veel tags. Hier staan de meeste tags op alfabetische volgorde met hun functie ernaast. De tags met een `*` mogen niet in de `<body>`

 Tag | Betekenis
-------------- | --------------------------
 `<!DOCTYPE>`* | Bepaalt het documenttype
 `<a>` | Bepaalt een hyperlink
 `<audio>` | Bepaalt geluidsinhoud
 `<b>` | Bepaalt vetgedrukte tekst
 `<body>`* | Bepaalt de body van het document
 `<br>` |  Bepaalt een enkele regeleinde
 `<button>` | Bepaalt een klikbare knop
 `<canvas>` | Heeft op zichzelf weinig gebruik, je kunt het samen met javascript gebruiken om afbeeldingen te tekenen, je kunt ook spelletjes maken.
 `<div>` | Bepaalt een sectie in een document
 `<embed>` | Bepaalt een container voor een externe (niet-HTML) applicatie
 `<footer>` | Bepaalt een voettekst voor een document of sectie
 `<form>` | Bepaalt een HTML-formulier voor gebruikersinvoer
 `<h1>` tot `<h6>` | Bepaalt HTML-koppen
 `<head>`* | Bepaalt informatie over het document
 `<header>` | Bepaalt een koptekst voor een document of sectie
 `<html>` | Bepaalt de hoofdmap van een HTML-document
 `<i>` | Bepaalt een deel van de tekst in een alternatieve stem of stemming
 `<iframe>` | Bepaalt een inline frame
 `<img>` | Bepaalt een afbeelding
 `<input>` | Bepaalt een invoerbesturing
 `<li>` | Bepaalt een lijstitem
 `<meta>`* | Bepaalt metagegevens over een HTML-document
 `<nav>` | Bepaalt navigatielinks
 `<noscript>` | Bepaalt een alternatieve inhoud voor gebruikers die geen scripts aan de clientzijde ondersteunen
 `<ol>` | Bepaalt een geordende lijst, dus een lijst met nummers ervoor. 1. 2. 3. enz.
 `<p>` | Bepaalt een alinea
 `<s>` | Bepaalt tekst die niet langer correct is
 `<script>` | Bepaalt een client-side script
 `<section>` | Bepaalt een sectie in een document
 `<span>` | Bepaalt een sectie in een document
 `<strong>` | Bepaalt belangrijke tekst
 `<style>` | Bepaalt stijlinformatie voor een document
 `<svg>` | Bepaalt een container voor SVG-afbeeldingen
 `<table>` | Bepaalt een tabel
 `<td>` | Bepaalt een cel in een tabel
 *`<title>` | Bepaalt een titel voor het document
 `<tr>` | Bepaalt een rij in een tabel
 `<u>` | Bepaalt tekst die stilistisch anders moet zijn dan de normale tekst
 `<ul>` | Bepaalt een ongeordende lijst, een lijst zonder nummers ervoor. * * * enz.
 `<video>` | Bepaalt een video
